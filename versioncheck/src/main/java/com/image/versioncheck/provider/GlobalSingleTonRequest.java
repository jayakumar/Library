package com.image.versioncheck.provider;

import android.support.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class GlobalSingleTonRequest {
    private static final String TAG = GlobalSingleTonRequest.class.getSimpleName();
    HashMap<String, String> params;
    String Url;
    int Method = Request.Method.POST;
    VolleyResponse volleyResponse;
    RequestQueue requestQueue;

    public interface VolleyResponse {
        void OnResponse(String response);

        void OnError(String error);
    }

    public GlobalSingleTonRequest(HashMap<String, String> params, @NonNull String url, VolleyResponse volleyResponse, @NonNull RequestQueue requestQueue) {

        this.params = params;
        Url = url;
        this.requestQueue = requestQueue;
        this.volleyResponse = volleyResponse;
        if (this.params == null) {
            this.params = new HashMap<>();
        }



    }



    public void doVolleyRequest() {
        CustomRequestLoader jsonObjectRequest = new CustomRequestLoader(Method, Url, params,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (volleyResponse != null) {
                    volleyResponse.OnResponse(response.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (volleyResponse != null) {
                    if (volleyError instanceof NoConnectionError) {
                        volleyResponse.OnError(" Check Your Internet Connection ");
                    } else if (volleyError instanceof TimeoutError) {
                        volleyResponse.OnError(" Connection Time out");
                    } else if (volleyError instanceof ServerError) {
                        volleyResponse.OnError("An error Occurred while executing");
                    } else if (volleyError instanceof AuthFailureError) {
                        volleyResponse.OnError("An error Occurred while executing");
                    } else if (volleyError instanceof NetworkError) {
                        volleyResponse.OnError("There is a problem with your network");
                    } else {
                        volleyResponse.OnError(volleyError.toString()
                                .replaceAll("com.android.volley.", ""));
                    }
                }
            }
        });
        requestQueue.add(jsonObjectRequest);

    }
}

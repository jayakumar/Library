package com.image.versioncheck.provider;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/***
 * Created by Jinu on 4/20/2016.
 ***/
public class CustomRequestLoader extends Request<JSONObject>{


    private Response.Listener<JSONObject> listener;
    private Map<String, String> params;

    public CustomRequestLoader(String url, Map<String, String> params, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, errorListener);
        this.listener = responseListener;
        this.params = params;

    }

    public CustomRequestLoader(int method, String url, Map<String, String> params, Response.Listener<JSONObject> reponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }


    @Override
    protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
        return params;
    }

    ;

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }


   /* @Override
    public String getBodyContentType() {
        return "Content-Type: application/json";
    }*/

    @Override
    protected void deliverResponse(JSONObject response) {
        listener.onResponse(response);
    }

}

package com.image.versioncheck.utils;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class SharedPreferenceId {

    public static final String VERSION = "version";
    public static final String UPDATE_AVAILABLE = "skip";
    public static final String SKIP = "skip_pressed";

}

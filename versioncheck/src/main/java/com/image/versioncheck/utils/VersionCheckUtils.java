package com.image.versioncheck.utils;

import android.content.Context;
import android.content.Intent;

import com.image.versioncheck.service.UpdateIntentService;

/***
 * Created by Jinu on 4/20/2016.
 ***/
public class VersionCheckUtils {

    private static VersionCheckUtils ourInstance = new VersionCheckUtils();
    private String projectName;
    private int versionCode = -1;
    private String url;
    private String bundleId;


    public static VersionCheckUtils getInstance() {
        return ourInstance;
    }

    private VersionCheckUtils() {
    }

    public VersionCheckUtils setProjectName(String projectName) {
        this.projectName = projectName;
        return ourInstance;
    }

    public VersionCheckUtils setVersionCode(int versionCode) {
        this.versionCode = versionCode;
        return ourInstance;
    }

    public VersionCheckUtils setUrl(String url) {
        this.url = url;
        return ourInstance;
    }


    public VersionCheckUtils setBundleId(String bundleId) {
        this.bundleId = bundleId;
        return ourInstance;
    }

    public void build(Context context) {
        Intent intent = new Intent(context, UpdateIntentService.class);
        intent.putExtra(UpdateIntentService.VERSION, versionCode);
        intent.putExtra(UpdateIntentService.PROJECT_NAME, projectName);
        intent.putExtra(UpdateIntentService.BUNDLE_ID, bundleId);
        intent.putExtra(UpdateIntentService.URL, url);
        context.startService(intent);
    }
}

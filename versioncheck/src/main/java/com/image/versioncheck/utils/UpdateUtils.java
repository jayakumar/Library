package com.image.versioncheck.utils;

import android.content.Context;

import com.jinu.versioncheck.BuildConfig;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class UpdateUtils {

    public static final String TAG = UpdateUtils.class.getSimpleName();


    public static boolean isUpdatedVersion(Context context, int version) {

        if (PreferenceUtils.getVersion(context)
                < version) {
            return true;
        }

        PreferenceUtils.setVersion(context, BuildConfig.VERSION_CODE);
        return false;
    }

    public static boolean isLock(Context context) {
        return PreferenceUtils.getUpdateMandatory(context);
    }
}

package com.image.versioncheck.utils;

import android.content.Context;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class PreferenceUtils {

    public static int getVersion(Context context) {
        return context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .getInt(SharedPreferenceId.VERSION, 0);
    }

    public static void setVersion(Context context, int versionCode) {
        context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .edit()
                .putInt(SharedPreferenceId.VERSION, versionCode)
                .apply();
    }


    public static boolean getUpdateMandatory(Context context) {

        return context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .getBoolean(SharedPreferenceId.UPDATE_AVAILABLE, true);
    }

    public static void setUpdateMandatory(Context context, boolean isUpdateAvailable) {
        context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(SharedPreferenceId.UPDATE_AVAILABLE, isUpdateAvailable)
                .apply();
    }

    public static void setSkip(Context context, boolean skip) {
        context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(SharedPreferenceId.SKIP, skip)
                .apply();
    }

    public static boolean getSkip(Context context) {
        return context.getSharedPreferences(SharedPreferenceId.VERSION, Context.MODE_PRIVATE)
                .getBoolean(SharedPreferenceId.SKIP, false);
    }
}

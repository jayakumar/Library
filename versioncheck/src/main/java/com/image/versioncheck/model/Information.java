
package com.image.versioncheck.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Information implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("projectName")
    @Expose
    private String projectName;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    @SerializedName("message")
    @Expose
    private String message;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName The projectName
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType The deviceType
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return The version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate The updatedDate
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Information() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.projectName);
        dest.writeString(this.deviceType);
        dest.writeString(this.version);
        dest.writeString(this.status);
        dest.writeString(this.updatedDate);
        dest.writeString(this.message);
    }

    protected Information(Parcel in) {
        this.id = in.readInt();
        this.projectName = in.readString();
        this.deviceType = in.readString();
        this.version = in.readString();
        this.status = in.readString();
        this.updatedDate = in.readString();
        this.message = in.readString();
    }

    public static final Creator<Information> CREATOR = new Creator<Information>() {
        @Override
        public Information createFromParcel(Parcel source) {
            return new Information(source);
        }

        @Override
        public Information[] newArray(int size) {
            return new Information[size];
        }
    };
}

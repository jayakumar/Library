
package com.image.versioncheck.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Result implements Parcelable {

    @SerializedName("Information")
    @Expose

    private Information Information;



    @SerializedName("userStatus")
    @Expose
    private String userStatus;

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * @return The Information
     */
    public Information getInformation() {
        return Information;
    }

    /**
     * @param Information The Information
     */
    public void setInformation(Information Information) {
        this.Information = Information;
    }


    public Result() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Information, flags);
        dest.writeString(this.userStatus);
    }

    protected Result(Parcel in) {
        this.Information = in.readParcelable(com.image.versioncheck.model.Information.class.getClassLoader());
        this.userStatus = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}

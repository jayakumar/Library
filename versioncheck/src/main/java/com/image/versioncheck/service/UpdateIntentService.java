package com.image.versioncheck.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.image.versioncheck.model.Information;
import com.image.versioncheck.model.Response;
import com.image.versioncheck.provider.GlobalSingleTonRequest;
import com.image.versioncheck.provider.VolleySingleton;
import com.image.versioncheck.ui.UpdateAppActivity;
import com.image.versioncheck.utils.PreferenceUtils;
import com.jinu.versioncheck.R;
import com.google.gson.Gson;

import java.util.HashMap;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class UpdateIntentService extends IntentService implements GlobalSingleTonRequest.VolleyResponse {

    public static final String TAG = UpdateIntentService.class.getSimpleName();


    public static final String URL = "url";
    public static final String VERSION = "version";
    public static final String PROJECT_NAME = "projectName";
    public static final String BUNDLE_ID = "bundleId";

    private static final String MANDATORY = "M";
    private static final String SKIP = "S";
    @SuppressWarnings("unused")
    private static final String DEFAULT = "D";
    private int version;


    public UpdateIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String url = intent.getExtras().getString(URL);
            int version = intent.getExtras().getInt(VERSION);
            String projectName = intent.getExtras().getString(PROJECT_NAME);
            String bundleId = intent.getExtras().getString(BUNDLE_ID);
            if (url == null) {
                Log.e(TAG, "onHandleIntent: ", new NullPointerException("Please provide url" +
                        "Use VersionCheckUtils.setUrl(<Your URL>)"));
            }
            if (projectName == null) {
                Log.e(TAG, "onHandleIntent: ", new NullPointerException("Please provide projectName" +
                        "Use VersionCheckUtils.setProjectName(<Your project name>)"));
            }
            if (version == -1) {
                Log.e(TAG, "onHandleIntent: ", new NullPointerException("Please provide version" +
                        "Use VersionCheckUtils.setVersionCode(<Your version>)"));
            }
            if (bundleId == null) {
                Log.e(TAG, "onHandleIntent: ", new NullPointerException("Please provide bundleId" +
                        "Use VersionCheckUtils.setBundleId(<Your bundle id>)"));
            }
            updateCheck(url, version, projectName, bundleId);
        }

    }

    private void updateCheck(String url, int version, String projectName, String bundleId) {
        this.version = version;
        HashMap<String, String> map = new HashMap<>();
        map.put("deviceType", "android");
        map.put("appVersion", version + ".0");
        map.put("projectName", projectName);
        map.put("bundleId", bundleId);
        new GlobalSingleTonRequest(map,
                url, this,
                VolleySingleton.getInstance(UpdateIntentService.this)
                        .getRequestQueue())
                .doVolleyRequest();
    }

    @Override
    public void OnResponse(String response) {
        Log.d(TAG, "OnResponse() called with: " + "response = [" + response + "]");
        try {
            Response result = new Gson().fromJson(response, Response.class);
            if (result.getResult().getUserStatus().equals(getString(R.string.download_available))) {
                Information information = result.getResult().getInformation();
                switch (information.getStatus()) {
                    case MANDATORY:
                        PreferenceUtils.setSkip(this, false);
                        launchActivity(version, true);
                        break;
                    case SKIP:
                        launchActivity(version, false);
                        break;
                    default:
                        stopSelf();
                        return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        stopSelf();
    }

    private void launchActivity(int version, boolean isMandatory) {

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (PreferenceUtils.getSkip(this) && !isMandatory) {
            stopSelf();
            return;
        }
        PreferenceUtils.setVersion(this, version);
        PreferenceUtils.setUpdateMandatory(this, isMandatory);
        startActivity(new Intent(UpdateIntentService.this, UpdateAppActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        stopSelf();
    }


    @Override
    public void OnError(String error) {
        stopSelf();
    }


}
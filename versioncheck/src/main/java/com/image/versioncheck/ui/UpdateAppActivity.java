package com.image.versioncheck.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.image.versioncheck.utils.PreferenceUtils;
import com.jinu.versioncheck.R;

/***
 * Created by Jinu on 4/18/2016.
 ***/
public class UpdateAppActivity extends AppCompatActivity implements View.OnClickListener {


    private Button mUpdate;
    private Button mSkipButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        setContentView(R.layout.activity_check_api_version);
        mUpdate = findViewById(R.id.update);
        mSkipButton = findViewById(R.id.skip);
        if (!PreferenceUtils.getUpdateMandatory(this)) {
            mSkipButton.setVisibility(View.VISIBLE);
        }
        assert mUpdate != null;
        mUpdate.setOnClickListener(this);
        mSkipButton.setOnClickListener(this);
        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mUpdate.getId()) {
            launchPlayStore();
        } else if (v.getId() == mSkipButton.getId()) {
            setSkip();
            finish();
        }
    }

    private void setSkip() {
        PreferenceUtils.setSkip(this, true);
    }

    private void launchPlayStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                    + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" +
                            appPackageName)));
        }
    }

    @Override
    public void onBackPressed() {
        if (PreferenceUtils.getUpdateMandatory(this)) {
            return;
        }
        super.onBackPressed();
    }
}
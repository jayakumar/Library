package fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.image.attacher.utils.ImagePicker;
import com.image.attacher.utils.ImageUtils;
import com.reusable.androidreusablecomponents.R;

/***
 * Created by Jinu on 4/22/2016.
 ***/
public class AttachImageFragment extends Fragment implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {

    private Button button;
    private ImageView imageView;
    private CheckBox use_camera;
    private CheckBox use_storage_api;
    private CheckBox save_public_dir;
    private boolean useGallery = true;
    private boolean useStorageApi = false;
    private boolean savePublicDir = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_attach_image, container, false);
    }


    @Override
    public void onViewCreated(View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button = view.findViewById(R.id.button);
        imageView = view.findViewById(R.id.imageView2);
        use_camera = view.findViewById(R.id.use_camera);
        use_storage_api = view.findViewById(R.id.use_storage_api);
        save_public_dir = view.findViewById(R.id.save_public_dir);
        button.setOnClickListener(this);
        use_camera.setOnCheckedChangeListener(this);
        use_storage_api.setOnCheckedChangeListener(this);
        save_public_dir.setOnCheckedChangeListener(this);
    }


    @Override
    public void onClick(View v) {
        startActivityForResult(ImagePicker.getInstance()
                .setFolderName("JinuImageAttachLib")
                .setCompressWidthHeight(500, 500)
                .useGallery(useGallery)
                .usePictureDir(savePublicDir)
                .useStorageApi(useStorageApi)
                .setIcon(R.drawable.ic_send_white_24dp)
                .build(getActivity()), 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            String fileLocation = data.getData().getPath();
            imageView.setImageBitmap(ImageUtils.decodeBitmap(fileLocation, 100, 100));
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (use_storage_api == buttonView) {
            useStorageApi = isChecked;
        }
        if (use_camera == buttonView) {
            useGallery = !useGallery;
        }
        if (save_public_dir == buttonView) {
            savePublicDir = isChecked;
        }
    }
}

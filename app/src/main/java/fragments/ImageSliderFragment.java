package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.reusable.androidreusablecomponents.R;

import java.util.ArrayList;

import imageslider.activity.ImageSliderActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImageSliderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageSliderFragment extends Fragment implements View.OnClickListener {

    private Button mImageSliderButton;

    public ImageSliderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of this fragment
     *
     * @return A new instance of fragment ImageSliderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageSliderFragment newInstance() {
        ImageSliderFragment fragment = new ImageSliderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_slider, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initViews(view);
        initController();
    }

    /**
     * Initialize the views
     *
     * @param view Instance of layout view
     */
    private void initViews(View view) {
        mImageSliderButton = (Button) view.findViewById(R.id.button_image_slider);
    }

    /**
     * Initialize the controller
     */
    private void initController() {
        mImageSliderButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_image_slider:
                ArrayList<String> imageArrayList = new ArrayList<>();
                imageArrayList.add("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTfZz_-WmmixVCxZfdvdWNTs6Yf2JwgFMOYuMkyt5-wUtEDHHpSEQ");
                imageArrayList.add("https://static-secure.guim.co.uk/sys-images/Sport/Pix/pictures/2010/12/19/1292778133565/Sachin-Tendulkar-India-So-007.jpg");
                imageArrayList.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTMvLzOic1Hf7odoK7Htqxrtp3am0JpU8QuKBp7RXG1zuF9EgA9PA");
                ImageSliderActivity.startIamgeSlider(getActivity(), imageArrayList);
                break;
        }
    }
}

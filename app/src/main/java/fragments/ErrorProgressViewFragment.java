package fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.errorprogress.ErrorProgressView;
import com.reusable.androidreusablecomponents.R;

/***
 * Created by Jinu on 4/22/2016.
 ***/
public class ErrorProgressViewFragment extends Fragment {

    ErrorProgressView errorProgressView;
    int count = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_error_progress_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        errorProgressView = (ErrorProgressView) view.findViewById(R.id.error_view);
        errorProgressView.setErrorType(ErrorProgressView.TYPE_EMPTY);
        errorProgressView.setOnRetryAction(new ErrorProgressView.OnRetryAction() {
            @Override
            public void onRetry() {
                errorProgressView.showProgress(true);
                new CountDownTimer(500, 100) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {

                        switch (count) {
                            case 1:
                                errorProgressView.setErrorType(ErrorProgressView.TYPE_NONE);
                                break;
                            case 2:
                                errorProgressView.setErrorType(ErrorProgressView.TYPE_EMPTY);
                                break;
                            default:
                                count = 0;
                                errorProgressView.setErrorType(ErrorProgressView.TYPE_EMPTY);
                                break;
                        }
                        count++;
                    }
                }.start();

            }
        });
    }
}

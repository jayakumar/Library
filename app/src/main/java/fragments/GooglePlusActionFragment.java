package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.reusable.androidreusablecomponents.R;

import googlemodule.activity.GoogleActionActivity;
import googlemodule.global.GooglePlusConfig;
import twitterModule.global.ConstantValues;

/**
 * Created by nidheesh on 3/4/2016.
 **/
public class GooglePlusActionFragment extends Fragment implements View.OnClickListener {

    /**
     * Implements various GooglePlus Actions
     * Button type view
     */
    private Button mLoginButton;
    private Button mLogoutButton;
    private Button mShareButton;


    public static GooglePlusActionFragment newInstance() {
        return new GooglePlusActionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_google_plus_action, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initController();
        initModel();
    }

    /**
     * Initialize the views
     *
     * @param view Instance of the main layout
     */
    private void initViews(View view) {
        mLoginButton = view.findViewById(R.id.google_button_login);
        mLogoutButton = view.findViewById(R.id.google_logout_button);
        mShareButton = view.findViewById(R.id.google_share_image_button);
    }

    /**
     * Initialize the controller
     */
    private void initController() {
        mLoginButton.setOnClickListener(this);
        mLogoutButton.setOnClickListener(this);
        mShareButton.setOnClickListener(this);
    }


    /**
     * Initialize the model
     */
    private void initModel() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.google_button_login:
                performGooglePlusLogin();
                break;
            case R.id.google_logout_button:
                performGooglePlusLogout();
                break;
            case R.id.google_share_image_button:
                performGooglePlusShare();
                break;
        }
    }

    private void performGooglePlusLogin() {
        GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
        googlePlusConfig.action = ConstantValues.Action.ACTION_LOGIN;
        GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
    }

    private void performGooglePlusLogout() {
        GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
        googlePlusConfig.action = ConstantValues.Action.ACTION_LOGOUT;
        GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
    }

    private void performGooglePlusShare() {
        GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
        googlePlusConfig.action = ConstantValues.Action.ACTION_SHARE;
        GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
    }


}

package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.image.attacher.utils.ZoomImageUtils;
import com.reusable.androidreusablecomponents.R;

/***
 * Created by Jinu on 4/23/2016.
 ***/
public class ImageZoomFragment extends Fragment implements View.OnClickListener {

    ImageView imageView;
    String url = "http://developer.android.com/training/material/images/SceneTransition.png";
    String title = "test";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_zoom, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = (ImageView) view.findViewById(R.id.image);
        Glide.with(getActivity())
                .load(url)
                .placeholder(com.image.attacher.R.drawable.placeholder)
                .crossFade()
                .into(imageView);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ZoomImageUtils
                .getInstance()
                .setTitle(title)
                .setUrl(url)
                .setFile(false)
                .build(imageView, getActivity());
    }
}

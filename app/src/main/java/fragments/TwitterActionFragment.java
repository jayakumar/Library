package fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.reusable.androidreusablecomponents.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import twitterModule.global.ConstantValues;
import twitterModule.provider.TwitterConfig;
import twitterModule.ui.OAuthActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TwitterActionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TwitterActionFragment extends Fragment implements View.OnClickListener {

    /**
     * Button for different twitter action Buttons
     */
    private Button mTwitterLogin;
    private Button mTwitterLogout;
    private Button mTwitterShareImage;


    public TwitterActionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment TwitterActionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TwitterActionFragment newInstance() {
        TwitterActionFragment fragment = new TwitterActionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_twitter_action, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initController();
    }

    /**
     * Initialize the views
     *
     * @param view Fragments main view
     */
    private void initViews(View view) {
        mTwitterLogin = (Button) view.findViewById(R.id.twitter_button_login);
        mTwitterLogout = (Button) view.findViewById(R.id.twitter_logout_button);
        mTwitterShareImage = (Button) view.findViewById(R.id.twitter_share_image_button);
    }

    /**
     * Initialize the action for each controller
     */
    private void initController() {
        mTwitterLogin.setOnClickListener(this);
        mTwitterLogout.setOnClickListener(this);
        mTwitterShareImage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.twitter_button_login:
                performLogin();
                break;
            case R.id.twitter_logout_button:
                break;
            case R.id.twitter_share_image_button:
                performShare();
                break;
        }
    }

    /**
     * Perform sharing of both image and text with Twitter
     */
    private void performShare() {
        String fileT = null;
        try {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.sample);
            File file = new File(getActivity().getCacheDir(), "sample.jpg");
            fileT = file.getAbsolutePath();
            FileOutputStream outStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        TwitterConfig twitterConfig = new TwitterConfig();
        twitterConfig.action = ConstantValues.Action.ACTION_SHARE;
        twitterConfig.fileLocation = fileT;
        twitterConfig.text = "Hi all.........";
        OAuthActivity.startTwitter(getActivity(), twitterConfig);
    }

    /**
     * Invoke twitter login request
     */
    private void performLogin() {
        TwitterConfig twitterConfig = new TwitterConfig();
        twitterConfig.action = ConstantValues.Action.ACTION_LOGIN;
        OAuthActivity.startTwitter(getActivity(), twitterConfig);
    }


    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }
}

package fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.widget.ShareDialog;
import com.reusable.androidreusablecomponents.R;
import com.reusable.reusablecomponent.FacebookActionActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FaceBookLoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FaceBookLoginFragment extends Fragment implements View.OnClickListener {


    /**
     * LoginButton botton widget for facebook login
     */
    private LoginButton mLoginButton;
    /**
     * Call back manger instance needed for login checking
     */
    private CallbackManager mCallbackManager;
    /**
     * An instance of ShareDialog
     */
    private ShareDialog mShareDialog;
    private Button mShareLinkButton;
    private Button mLoginUser;
    private Button mLogoutButton;
    private Button mShareImageButton;
    private static final int LOGIN_REQUEST_CODE = 5000;
    private static final int SHARE_URL_REQUEST_CODE = 6000;
    private static final int SHARE_IMAGE_REQUEST_CODE = 7000;
    private static final int LOGOUT_REQUEST_CODE = 11000;


    public FaceBookLoginFragment() {
        // Required empty public constructor
    }

    /**
     * @return A new instance of fragment FaceBookLoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FaceBookLoginFragment newInstance() {
        FaceBookLoginFragment fragment = new FaceBookLoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_face_book_login, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity());
        if (FacebookSdk.isInitialized()) {
            initViews(view);
            initControllers();
        }
    }

    private void initViews(View view) {
        mShareLinkButton = (Button) view.findViewById(R.id.share_link_button);
        mLoginUser = (Button) view.findViewById(R.id.normal_button_login);
        mLogoutButton = (Button) view.findViewById(R.id.logout_button);
        mShareImageButton = (Button) view.findViewById(R.id.share_image_button);
    }


    private void initControllers() {
        mShareLinkButton.setOnClickListener(this);
        mLoginUser.setOnClickListener(this);
        mLogoutButton.setOnClickListener(this);
        mShareImageButton.setOnClickListener(this);

    }

    private void shareLInks() {

        startActivityForResult(FacebookActionActivity.getShareLinkIntent(getActivity(), "Hello Facebook", "The 'Hello Facebook' sample  showcases simple Facebook integration", "http://developers.facebook.com/android"), SHARE_URL_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == LOGIN_REQUEST_CODE) {
                showSnackBar("Logged in successfully");
            } else if (requestCode == SHARE_URL_REQUEST_CODE) {
                showSnackBar("Link shared successfully");

            } else if (requestCode == SHARE_IMAGE_REQUEST_CODE) {
                showSnackBar("Image shared successfully");

            } else if (requestCode == LOGOUT_REQUEST_CODE) {

                showSnackBar("Logged out successfully");
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share_link_button:
                shareLInks();
                break;
            case R.id.normal_button_login:
                startActivityForResult(FacebookActionActivity.getLoginIntent(getActivity()), LOGIN_REQUEST_CODE);
                getActivity().overridePendingTransition(0, 0);
                break;
            case R.id.logout_button:
                doLogout();
                break;
            case R.id.share_image_button:
                shareImageToFaceBook();
                break;
        }
    }

    public void shareImageToFaceBook() {
        Bitmap image = BitmapFactory.decodeResource(getResources(), com.reusable.reusablecomponent.R.drawable.ic_launcher);
        Intent intent = FacebookActionActivity.getShareImageIntent(getActivity(), image, "Hello");
        startActivityForResult(intent, SHARE_IMAGE_REQUEST_CODE);
        getActivity().overridePendingTransition(0, 0);

    }

    public void doLogout() {
        Intent logoutIntent = FacebookActionActivity.getLogoutIntent(getActivity());
        startActivityForResult(logoutIntent, LOGOUT_REQUEST_CODE);
        getActivity().overridePendingTransition(0, 0);
    }

    /**
     * Shows snack bar for error condition
     *
     * @param message Error message to be shown
     */
    private void showSnackBar(String message) {

        Snackbar.make(getActivity().findViewById(R.id.facebook_coordinator), message, Snackbar.LENGTH_LONG).show();
    }


}

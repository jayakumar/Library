package twitterModule.provider;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitterModule.global.ConstantValues;

/***
 * Created by Jinu on 1/25/2016.
 ***/
public class TweetAsync extends AsyncTask<String, Void, Status> {

    private String mToken;
    private String mTokenSecret;
    private String mTextToTweet;
    private String mFileToTweet;
    private StatusChangedListener mListener;

    public interface StatusChangedListener {
        void onStatusChange(boolean status);
    }

    public TweetAsync(String mToken, String mTokenSecret, String mTextToTweet, String mFileToTweet, StatusChangedListener mListener) {
        this.mToken = mToken;
        this.mTokenSecret = mTokenSecret;
        this.mTextToTweet = mTextToTweet;
        this.mFileToTweet = mFileToTweet;
        this.mListener = mListener;
    }

    @Override
    protected twitter4j.Status doInBackground(String... params) {
        try {

            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.setOAuthConsumerKey(ConstantValues.TWITTER_CONSUMER_KEY);
            configurationBuilder.setOAuthConsumerSecret(ConstantValues.TWITTER_CONSUMER_SECRET);
            configurationBuilder.setOAuthAccessToken(mToken);
            configurationBuilder.setOAuthAccessTokenSecret(mTokenSecret);
            Twitter twitter = new TwitterFactory(configurationBuilder.build()).getInstance();
            StatusUpdate statusUpdate = new StatusUpdate(mTextToTweet);
            if (mFileToTweet != null) {
                File file = new File(mFileToTweet);
                statusUpdate.setMedia(file);
            }
            twitter4j.Status response = twitter.updateStatus(statusUpdate);
            Log.d("Status", response.getText());
            return response;

        } catch (TwitterException e) {
            e.printStackTrace();

        }
        return null;
    }


    @Override
    protected void onPostExecute(twitter4j.Status status) {
        super.onPostExecute(status);
        mListener.onStatusChange(status != null);
    }


}
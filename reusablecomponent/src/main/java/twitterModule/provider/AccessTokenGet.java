package twitterModule.provider;

import android.os.AsyncTask;

import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/***
 * Created by Jinu on 1/25/2016.
 ***/
public class AccessTokenGet extends AsyncTask<String, Void, Boolean> {


    private TokenChangedListener mListener;
    private RequestToken mRequestToken;
    private AccessToken mAccessToken;


    public interface TokenChangedListener {
        void onTokenGet(AccessToken accessToken, boolean status);
    }

    public AccessTokenGet(TokenChangedListener statusChangedListener, RequestToken mRequestToken) {
        this.mListener = statusChangedListener;
        this.mRequestToken = mRequestToken;

    }

    @Override
    protected Boolean doInBackground(String... args) {

        try {
            if (args != null) {
                mAccessToken = TwitterUtil.getInstance().getTwitter().getOAuthAccessToken(mRequestToken, args[0]);
            }

        } catch (TwitterException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        mListener.onTokenGet(mAccessToken, response);
    }
}
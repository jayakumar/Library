package twitterModule.provider;

import android.content.Context;

import twitterModule.global.ConstantValues;


/***
 * Created by Jinu on 1/25/2016.
 ***/
public class SharedPrefrenceUtils {

    public static String getString(Context context, String key) {
        return context.getSharedPreferences(ConstantValues.PREFERENCE_TWITTER, Context.MODE_PRIVATE)
                .getString(key, "");
    }

    public static boolean getBoolean(Context context, String key) {
        return context.getSharedPreferences(ConstantValues.PREFERENCE_TWITTER, Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }

    public static void putString(Context context, String key, String value) {
        context.getSharedPreferences(ConstantValues.PREFERENCE_TWITTER, Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    public static void putBoolean(Context context, String key, boolean value) {
        context.getSharedPreferences(ConstantValues.PREFERENCE_TWITTER, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(key, value)
                .apply();
    }
}

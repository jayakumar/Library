package twitterModule.provider;

import android.os.Parcel;
import android.os.Parcelable;

/***
 * Created by Jinu on 1/25/2016.
 ***/
public class TwitterConfig implements Parcelable {

    public String action;
    public String fileLocation;
    public String text;

    public TwitterConfig() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.action);
        dest.writeString(this.fileLocation);
        dest.writeString(this.text);
    }

    protected TwitterConfig(Parcel in) {
        this.action = in.readString();
        this.fileLocation = in.readString();
        this.text = in.readString();
    }

    public static final Creator<TwitterConfig> CREATOR = new Creator<TwitterConfig>() {
        public TwitterConfig createFromParcel(Parcel source) {
            return new TwitterConfig(source);
        }

        public TwitterConfig[] newArray(int size) {
            return new TwitterConfig[size];
        }
    };
}

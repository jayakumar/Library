package twitterModule.provider;

import android.os.AsyncTask;

import twitter4j.auth.RequestToken;

/***
 * Created by Jinu on 1/25/2016.
 ***/
public class GetRequestToken extends AsyncTask<String, String, RequestToken> {

    private String authenticationUrl;

    private RequestTokenListener mListener;

    public GetRequestToken(RequestTokenListener mListener) {
        this.mListener = mListener;
    }

    public interface RequestTokenListener {
        void onToken(RequestToken requestToken, String authenticationUrl);
    }


    @Override
    protected void onPostExecute(RequestToken requestToken) {
        mListener.onToken(requestToken, authenticationUrl);
    }

    @Override
    protected RequestToken doInBackground(String... params) {
        RequestToken token = TwitterUtil.getInstance().getRequestToken();
        if (token!=null) {
            authenticationUrl = token.getAuthenticationURL();
        }
        return token;
    }
}
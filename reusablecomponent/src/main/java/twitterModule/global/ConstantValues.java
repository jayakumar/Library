package twitterModule.global;

/***
 * Created by Jinu on 1/25/2016.
 ***/
public class ConstantValues {

    public static final String STRING_EXTRA_TWITTER_REQ_TOKEN = "STRING_EXTRA_TWITTER_REQ_TOKEN";

    // TODO: 1/25/2016 change your consumer key
    public static String TWITTER_CONSUMER_KEY = "h4wRN2YvkvOuysWnhrqcumwQU";

    // TODO: 1/25/2016 change your consumer secret
    public static String TWITTER_CONSUMER_SECRET = "wKAqwrDorEPTyvuIvTMD9cmdUZdkF3v43K6cEXxxQldakYSNbY";
    public static String TWITTER_CALLBACK_URL = "https://www.google.com";
    public static String URL_PARAMETER_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    public static String PREF_TWITTER_OAUTH_TOKEN = "TWITTER_OAUTH_TOKEN";
    public static String PREF_TWITTER_OAUTH_TOKEN_SECRET = "TWITTER_OAUTH_TOKEN_SECRET";
    public static String PREF_TWITTER_IS_LOGGED_IN = "TWITTER_IS_LOGGED_IN";
    public static String PREFERENCE_TWITTER = "PREFERENCE_TWITTER";

    public static String STRING_EXTRA_AUTHENCATION_URL = "anything://twitter";
    public static String STRING_EXTRA_TWITTER_ACTION = "ACTION";
    public static String STRING_EXTRA_GOOGLE_ACTION = "ACTION";


    public static class Action {
        public static final String ACTION_LOGIN = "LOGIN";
        public static final String ACTION_SHARE = "SHARE";
        public static final String ACTION_LOGOUT = "LOGOUT";
    }
}

package twitterModule.ui;


import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.reusable.reusablecomponent.R;

import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitterModule.global.ConstantValues;
import twitterModule.interfaces.TwitterCallBack;
import twitterModule.provider.AccessTokenGet;
import twitterModule.provider.SharedPrefrenceUtils;


/***
 * Created by Jinu on 1/25/2016.
 ***/
@SuppressWarnings("ConstantConditions")
public class OAuthWebViewFragment extends Fragment implements AccessTokenGet.TokenChangedListener {

    private WebView mWebView;
    private String mAuthUrl;
    private RequestToken mRequestToken;
    private TwitterCallBack mTwitterCallBack;
    private ProgressBar mProgressBar;

    public OAuthWebViewFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuthUrl = getArguments().getString(ConstantValues.STRING_EXTRA_AUTHENCATION_URL);
        mRequestToken = (RequestToken) getArguments().getSerializable(ConstantValues.STRING_EXTRA_TWITTER_REQ_TOKEN);
        performLoginAction();

    }


    @SuppressLint("SetJavaScriptEnabled")
    private void performLoginAction() {
        mWebView.loadUrl(mAuthUrl);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("oauth_verifier")) {
                    Uri uri = Uri.parse(url);
                    notifyLogin(uri.getQueryParameter("oauth_verifier"));
                    mProgressBar.setVisibility(View.INVISIBLE);
                } else if (url.contains(ConstantValues.TWITTER_CALLBACK_URL + "?den")) {
                    view.loadUrl(mAuthUrl);
                } else {
                    view.loadUrl(url);
                }

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void notifyLogin(String url) {
        new AccessTokenGet(this, mRequestToken).execute(url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.oauth_webview, container, false);
        mWebView = view.findViewById(R.id.webViewOAuth);
        mProgressBar = view.findViewById(R.id.progressBar);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTwitterCallBack = (TwitterCallBack) context;
    }


    private void writeToPreferences(AccessToken requestToken) {
        if (requestToken!=null) {
            if (!TextUtils.isEmpty(requestToken.getToken())&&!TextUtils.isEmpty(requestToken.getTokenSecret())) {
                SharedPrefrenceUtils.putString(getActivity(),
                        ConstantValues.PREF_TWITTER_OAUTH_TOKEN, requestToken.getToken());
                SharedPrefrenceUtils.putString(getActivity(),
                        ConstantValues.PREF_TWITTER_OAUTH_TOKEN_SECRET, requestToken.getTokenSecret());

            }
        }
    }

    @Override
    public void onTokenGet(AccessToken accessToken, boolean status) {
            showSnackBarAndDismiss("Twitter logged in  successfully",accessToken,status);
    }

    /**
     * Shows snack bar for error condition
     *
     * @param message Error message to be shown
     */
    private void showSnackBarAndDismiss(String message, final AccessToken accessToken, final boolean status) {

        Snackbar.make(getActivity().findViewById(R.id.twitter_webview_coordinator), message, Snackbar.LENGTH_LONG).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                writeToPreferences(accessToken);
                mTwitterCallBack.onLogin(status);
            }
        }, 3250);

    }



}

package twitterModule.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.reusable.reusablecomponent.R;

import twitter4j.auth.RequestToken;
import twitterModule.global.ConstantValues;
import twitterModule.interfaces.TwitterCallBack;
import twitterModule.provider.GetRequestToken;
import twitterModule.provider.SharedPrefrenceUtils;
import twitterModule.provider.TweetAsync;
import twitterModule.provider.TwitterConfig;
import utils.AndroidUtils;


/***
 * Created by Jinu on 1/25/2016.
 ***/
public class OAuthActivity extends AppCompatActivity implements

        TwitterCallBack,
        GetRequestToken.RequestTokenListener,
        TweetAsync.StatusChangedListener {

    /**
     * Request code for runtime permission
     */
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 125;
    /**
     * Specifies the user action
     */
    private String mAction = ConstantValues.Action.ACTION_SHARE;

    /**
     * Instance of  TwitterConfig to store data received from app side
     */
    private TwitterConfig mTwitterConfig;

    /**
     * Starts OAuthActivity based on the user action
     *
     * @param context       current context
     * @param twitterConfig data received from app side
     */
    public static void startTwitter(Activity context, TwitterConfig twitterConfig) {
        Intent intent = new Intent(context, OAuthActivity.class);
        intent.putExtra(ConstantValues.STRING_EXTRA_TWITTER_ACTION, twitterConfig);
        context.startActivity(intent);
        context.overridePendingTransition(0, 0);


    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauth);
        getData();
        initData();
    }

    /**
     * Take appropriate action according to user login status
     */
    private void initData() {
        boolean isLoggedIn = SharedPrefrenceUtils.getBoolean(OAuthActivity.this, ConstantValues.PREF_TWITTER_IS_LOGGED_IN);
        initProgress();
        if (mAction.equals(ConstantValues.Action.ACTION_SHARE)) {
            if (isLoggedIn) {
                handleActionShare();
            } else {
                new GetRequestToken(this).execute();
            }
        } else if (mAction.equals(ConstantValues.Action.ACTION_LOGIN)) {
            if (!isLoggedIn) {
                new GetRequestToken(this).execute();
            } else {

                AndroidUtils.hideProgressDialog();
                showSnackBarAndDismiss("Already logged in");

            }
        }

    }

    /**
     * Shows the progress dialog
     */
    private void initProgress() {
        AndroidUtils.showProgressDialog(this);
    }

    /**
     * Handle share action according to the android versions
     */
    private void handleActionShare() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermission();
        } else {
            tweet();
        }
    }

    /**
     * Tweet the specified image and text
     */
    private void tweet() {
        initProgress();
        String token = SharedPrefrenceUtils.getString(OAuthActivity.this, ConstantValues.PREF_TWITTER_OAUTH_TOKEN);
        String tokenSecret = SharedPrefrenceUtils.getString(OAuthActivity.this, ConstantValues.PREF_TWITTER_OAUTH_TOKEN_SECRET);
        new TweetAsync(token, tokenSecret, mTwitterConfig.text, mTwitterConfig.fileLocation, this).execute();
    }

    /**
     * Handles the runtime permission for Android M
     */
    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(OAuthActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            new GetRequestToken(this).execute();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
            }
        }
    }


    @Override
    public void onLogin(boolean status) {
        showSnackBar("login status : " + status);
        SharedPrefrenceUtils.putBoolean(OAuthActivity.this, ConstantValues.PREF_TWITTER_IS_LOGGED_IN, true);
        if (mAction.equals(ConstantValues.Action.ACTION_SHARE)) {
            tweet();
        } else if (mAction.equals(ConstantValues.Action.ACTION_LOGIN)) {
            AndroidUtils.hideProgressDialog();
            onSuccessFullLogin();
        }
    }


    /**
     * Show snack bar for success full login
     */
    private void onSuccessFullLogin() {
        showSnackBarAndDismiss("Login Success");
    }

    /**
     * @param status boolean value for indicating share status
     */
    public void onShare(boolean status) {
        if (status) {
            showSnackBarAndDismiss("Tweeted successfully");
            Toast.makeText(this, "Tweeted successfully", Toast.LENGTH_LONG).show();

        } else {
            showSnackBarAndDismiss("Tweet failed");

        }

        // TODO: 1/25/2016 apply your logic here
    }

    @Override
    public void onToken(RequestToken requestToken, String authenticationUrl) {
        if (requestToken != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString(ConstantValues.STRING_EXTRA_AUTHENCATION_URL,
                    authenticationUrl);
            bundle.putString(ConstantValues.STRING_EXTRA_TWITTER_ACTION,
                    mAction);
            bundle.putSerializable(ConstantValues.STRING_EXTRA_TWITTER_REQ_TOKEN,
                    requestToken);
            OAuthWebViewFragment oAuthWebViewFragment = new OAuthWebViewFragment();
            oAuthWebViewFragment.setArguments(bundle);
            fragmentTransaction.add(android.R.id.content, oAuthWebViewFragment);
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            showSnackBar("Please check your network");
        }
        AndroidUtils.hideProgressDialog();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_STORAGE) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new GetRequestToken(this).execute();
            } else {
                showSnackBar("Permission not available for sharing");
            }

        }
    }

    @Override
    public void onStatusChange(boolean status) {
        onShare(status);
        AndroidUtils.hideProgressDialog();
    }

    /**
     * Get data from intent from app
     */
    public void getData() {
        mTwitterConfig = getIntent().getExtras().getParcelable(ConstantValues.STRING_EXTRA_TWITTER_ACTION);
        assert mTwitterConfig != null;
        mAction = mTwitterConfig.action;

    }

    /**
     * Shows snack bar for error condition
     *
     * @param message Error message to be shown
     */
    private void showSnackBarAndDismiss(String message) {

        Snackbar.make(OAuthActivity.this.findViewById(R.id.twitter_library_coordinator), message, Snackbar.LENGTH_LONG).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                overridePendingTransition(0, 0);
            }
        }, 3250);

    }

    /**
     * Shows snack bar for error condition
     *
     * @param message Error message to be shown
     */
    private void showSnackBar(String message) {

        Snackbar.make(OAuthActivity.this.findViewById(R.id.twitter_library_coordinator), message, Snackbar.LENGTH_LONG).show();
    }
}

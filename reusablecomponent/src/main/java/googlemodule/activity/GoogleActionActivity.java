package googlemodule.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.PlusShare;
import com.reusable.reusablecomponent.R;

import googlemodule.global.GooglePlusConfig;
import twitterModule.global.ConstantValues;

public class GoogleActionActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    /**
     * TAG used to identify log
     */
    private static final String TAG = "SignInActivity";
    /**
     * Constants for indicating SignIn request
     */
    private static final int RC_SIGN_IN = 9001;
    /**
     * Instance of GoogleApiClient
     */
    private GoogleApiClient mGoogleApiClient;
    /**
     * ProgressDialog type view used to indicates progress of an action
     */
    private ProgressDialog mProgressDialog;
    /**
     * Model class implements Parcelable.Used pass basic data with intent from app to library
     */
    private GooglePlusConfig mGooglePlusConfig;
    /**
     * Denotes user users current action
     */
    private String mAction = ConstantValues.Action.ACTION_SHARE;
    /**
     * Request code for image picking Intent
     */
    private static final int REQ_SELECT_PHOTO = 1;
    /**
     * Request code for share intent
     */
    private static final int REQ_START_SHARE = 2;


    /**
     * Factory method to start activity with corresponding data from app
     *
     * @param context          App's current context
     * @param googlePlusConfig User action data.Contains the values for user action
     */
    public static void startGooglePlus(Activity context, GooglePlusConfig googlePlusConfig) {
        Intent intent = new Intent(context, GoogleActionActivity.class);
        intent.putExtra(ConstantValues.STRING_EXTRA_GOOGLE_ACTION, googlePlusConfig);
        context.startActivity(intent);
        context.overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_action);
        initGooglePlus();


    }

    /**
     * Get the data from intent
     */
    private void getData() {
        mGooglePlusConfig = getIntent().getExtras().getParcelable(ConstantValues.STRING_EXTRA_TWITTER_ACTION);
        assert mGooglePlusConfig != null;
        mAction = mGooglePlusConfig.action;
        initAction();

    }

    /**
     * Initialize the Google Plus components
     */
    private void initGooglePlus() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        if (mGoogleApiClient == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            // [END configure_signin]
            // [START build_client]
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }


        getData();


    }

    /**
     * Initialize the action according to the mAction values
     */
    private void initAction() {
        if (mAction.equals(ConstantValues.Action.ACTION_LOGIN)) {
            signIn();
        } else if (mAction.equals(ConstantValues.Action.ACTION_LOGOUT)) {
            signIn();
        }
        if (mAction.equals(ConstantValues.Action.ACTION_SHARE)) {
            pickImage();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == REQ_SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    shareToGooglePlus(data);
                }
            }
        } else if (requestCode == REQ_START_SHARE) {
            if (resultCode == RESULT_OK) {
                finish();
                overridePendingTransition(0, 0);
            } else if (resultCode == RESULT_CANCELED) {
                finish();
                overridePendingTransition(0, 0);
            }
        }
    }


    /**
     * @param result GoogleSignInResult contains the values for user signIn status
     */
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    /**
     * Enable user signIn
     */
    private void signIn() {


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    /**
     * Enable signOut from Google plus
     */
    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // [START_EXCLUDE]
                            updateUI(false);
                            // [END_EXCLUDE]
                        }
                    });


        } else {
            Toast.makeText(this, "Connection failed", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    /**
     * Shows the ProgressDialog
     */
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.please_wait));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    /**
     * Hide progress dialog
     */
    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    /**
     * Choose action according to the status
     *
     * @param signedIn boolean type for indicating the user signed in status
     */
    private void updateUI(boolean signedIn) {
        if (signedIn) {
            if (mAction.equals(ConstantValues.Action.ACTION_LOGIN)) {
                Toast.makeText(this, "Signed in", Toast.LENGTH_LONG).show();
                finish();
                overridePendingTransition(0, 0);
            } else if (mAction.equals(ConstantValues.Action.ACTION_LOGOUT)) {
                signOut();
            }
        } else {
            if (mAction.equals(ConstantValues.Action.ACTION_LOGOUT)) {
                Toast.makeText(this, "Signed out", Toast.LENGTH_LONG).show();
                finish();
                overridePendingTransition(0, 0);
            }

        }
    }

    /**
     * Pick image of video from gallery using intent.
     * The request code is REQ_SELECT_PHOTO
     */
    private void pickImage() {
        Intent photoPicker = new Intent(Intent.ACTION_PICK);
        photoPicker.setType("video/*, image/*");
        startActivityForResult(photoPicker, REQ_SELECT_PHOTO);
    }

    /**
     * Share both image and text
     *
     * @param intent Intent contain the data retrieved from image picker request
     */
    private void shareToGooglePlus(Intent intent) {
        Uri selectedImage = intent.getData();
        ContentResolver cr = this.getContentResolver();
        String mime = cr.getType(selectedImage);

        PlusShare.Builder share = new PlusShare.Builder(this);
        share.setText("hello everyone!");
        share.addStream(selectedImage);
        share.setType(mime);
        startActivityForResult(share.getIntent(), REQ_START_SHARE);
    }

}
package googlemodule.global;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nidheesh on 3/4/2016.
 **/
public class GooglePlusConfig implements Parcelable {

    public String action;
    public String fileLocation;
    public String text;

    public GooglePlusConfig() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.action);
        dest.writeString(this.fileLocation);
        dest.writeString(this.text);
    }

    protected GooglePlusConfig(Parcel in) {
        this.action = in.readString();
        this.fileLocation = in.readString();
        this.text = in.readString();
    }

    public static final Creator<GooglePlusConfig> CREATOR = new Creator<GooglePlusConfig>() {
        public GooglePlusConfig createFromParcel(Parcel source) {
            return new GooglePlusConfig(source);
        }

        public GooglePlusConfig[] newArray(int size) {
            return new GooglePlusConfig[size];
        }
    };
}
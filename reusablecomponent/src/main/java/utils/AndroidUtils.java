package utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;

import com.reusable.reusablecomponent.R;

/**
 * Created by nidheesh on 3/1/2016.
 **/
public class AndroidUtils {

    /**
     * progressDialog ,for indicate the progress of background task
     */
    public static ProgressDialog progressDialog;


    /**
     * Check for progress dialog is exist.If not create new
     * @param context Instance of active context
     */
    public static  void showProgressDialog(Context context) {
        if (progressDialog == null) {
            progressDialog = createProgressDialog(context);
            progressDialog.show();
        } else {
            progressDialog.show();
        }
    }

    /**
     * Returns the ProgressDialog instance
     * @param mContext Instance of active context
     * @return ProgressDialog instace
     */
    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {

        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_progress_dialog);
        return dialog;
    }

    /**
     * Hides the progress dialog
     */
    public static void hideProgressDialog() {

        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}

package utils;

import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validation {


    private static final String TAG = "Validation";

    public static class ErrorValues {
        public static final int INVALID = 1;
        public static final int EMPTY = 2;
        public static final int EMPTY_VIEW = 3;
        public static final int OK = 4;
    }

    public String phone_pattern = "\\d{10}";


    public String email_pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
            + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
            + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";

    public int isAlphaNumeric(String s) {
        if (isEmpty(s)) {
            return ErrorValues.EMPTY;
        }
        String pattern = "^[a-zA-Z0-9]*$";
        return s.matches(pattern) ? ErrorValues.OK : ErrorValues.EMPTY;
    }

    public int containsOnlyNumbers(String str) {
        if (isEmpty(str)) {
            return ErrorValues.EMPTY;
        }
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return ErrorValues.INVALID;
        }
        return ErrorValues.OK;
    }

    private boolean isEmpty(String str) {
        return TextUtils.isEmpty(str);
    }


    @SuppressWarnings("SimplifiableIfStatement")
    public int phone(EditText phone) {
        if (checkEditText(phone)) {
            return ErrorValues.EMPTY_VIEW;
        }
        if (isEmpty(phone.getText().toString())) {
            return ErrorValues.EMPTY;
        }
        Pattern phoneNumber = Pattern.compile(phone_pattern);
        Matcher matcher1 = phoneNumber.matcher(phone.getText().toString());

        if (!matcher1.matches()) {
            return ErrorValues.INVALID;
        }
        return ErrorValues.OK;

    }

    @SuppressWarnings("RedundantIfStatement")
    public int email(EditText email) {
        if (checkEditText(email)) {
            return ErrorValues.EMPTY_VIEW;
        }
        if (isEmpty(email.getText().toString())) {
            return ErrorValues.EMPTY;
        }
        if (!email.getText().toString().matches(email_pattern)) {
            return ErrorValues.INVALID;
        }
        return ErrorValues.OK;

    }


    public int name(EditText name) {
        if (checkEditText(name)) {
            return ErrorValues.EMPTY_VIEW;
        }
        if (isEmpty(name.getText().toString())) {
            return ErrorValues.EMPTY;
        }
        if (!name.getText().toString().matches("^[a-zA-Z\\s]+")) {
            return ErrorValues.INVALID;
        }
        return ErrorValues.OK;

    }


    public int isAlphaNumeric(EditText s) {
        if (checkEditText(s)) {
            return ErrorValues.EMPTY_VIEW;
        }
        if (isEmpty(s.getText().toString())) {
            return ErrorValues.EMPTY;
        }
        String pattern = "^[a-zA-Z0-9]*$";

        if (!s.getText().toString().matches(pattern)) {
            return ErrorValues.INVALID;
        }

        return ErrorValues.OK;
    }

    private boolean checkEditText(EditText s) {
        if (s == null) {
            Log.e(TAG, "isAlphaNumeric: ", new NullPointerException("Do you declared your edit text"));
            return true;
        }
        return false;
    }


}

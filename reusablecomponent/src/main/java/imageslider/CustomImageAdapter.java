package imageslider;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reusable.reusablecomponent.R;

import java.util.ArrayList;

/**
 * Created by nidheesh on 3/8/2016.
 **/
public class CustomImageAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    private ArrayList<String> mImageList;
    public CustomImageAdapter(Context context,ArrayList<String> imageList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mImageList=imageList;
    }

    @Override
    public int getCount() {
        if(mImageList!=null&&mImageList.size()>0) {
            return mImageList.size();
        }
        else
        {
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        String imageUrl=mImageList.get(position);
        Glide.with(mContext).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholderimage).into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
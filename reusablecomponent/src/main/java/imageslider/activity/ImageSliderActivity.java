package imageslider.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.reusable.reusablecomponent.R;

import java.util.ArrayList;

import imageslider.CustomImageAdapter;

public class ImageSliderActivity extends AppCompatActivity {

    /**
     * Intent key passing image arraylist
     */
    private static  String INTENT_KEY="image_intent";
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);
        initViews();
        initModels();
    }
    public static void startIamgeSlider(Activity activity,ArrayList<String> imageArrayList){
        Intent intent=new Intent(activity,ImageSliderActivity.class);
        intent.putStringArrayListExtra(INTENT_KEY, imageArrayList);
        activity.startActivity(intent);
    }

    private void initViews(){
        mViewPager= findViewById(R.id.image_viewer);
    }
    private void initModels(){
        ArrayList<String> imageArrayList=getIntent().getStringArrayListExtra(INTENT_KEY);
        if (imageArrayList!=null&&imageArrayList.size()>0){
            CustomImageAdapter customImageAdapter=new CustomImageAdapter(this,imageArrayList);
            mViewPager.setAdapter(customImageAdapter);
        }
    }
}

package com.reusable.reusablecomponent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;

import utils.AndroidUtils;

public class FacebookActionActivity extends AppCompatActivity {

    /**
     * Call back manger instance needed for login checking
     */
    private CallbackManager mCallbackManager;
    private static final String INTENT_KEY = "action";
    private static final String TITLE_KEY = "title_key";
    private static final String DESCRIPTION_KEY = "description_key";
    private static final String URL_KEY = "url_key";
    private static final int LOGIN_INTENT = 1000;
    private static final int SHARE_URL_INTENT = 2000;
    private static final int SHARE_IMAGE_INTENT = 3000;
    private static final String IMAGE_KEY = "image";
    private static final int LOGOUT_INTENT = 9000;


    ShareDialog mShareDialog;

    public static Intent getLoginIntent(Context context) {
        Intent loginIntent = new Intent(context, FacebookActionActivity.class);
        loginIntent.putExtra(INTENT_KEY, LOGIN_INTENT);
        return loginIntent;
    }

    public static Intent getShareLinkIntent(Context context, String title, String description, String url) {
        Intent shareLinkIntent = new Intent(context, FacebookActionActivity.class);
        shareLinkIntent.putExtra(INTENT_KEY, SHARE_URL_INTENT);
        shareLinkIntent.putExtra(TITLE_KEY, title);
        shareLinkIntent.putExtra(DESCRIPTION_KEY, description);
        shareLinkIntent.putExtra(URL_KEY, url);
        return shareLinkIntent;
    }

    public static Intent getShareImageIntent(Context context, Bitmap bitmap, String title) {
        Intent shareImageIntent = new Intent(context, FacebookActionActivity.class);
        shareImageIntent.putExtra(INTENT_KEY, SHARE_IMAGE_INTENT);
        shareImageIntent.putExtra(IMAGE_KEY, bitmap);
        shareImageIntent.putExtra(TITLE_KEY, title);
        return shareImageIntent;

    }

    public static Intent getLogoutIntent(Context context) {
        Intent logoutIntent = new Intent(context, FacebookActionActivity.class);
        logoutIntent.putExtra(INTENT_KEY, LOGOUT_INTENT);
        return logoutIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_action);
        mCallbackManager = CallbackManager.Factory.create();
        mShareDialog = new ShareDialog(this);
        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    setResult(RESULT_OK);
                    finish();
                    overridePendingTransition(0, 0);
                    AndroidUtils.hideProgressDialog();
                }
            }
        };
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            switch (bundle.getInt(INTENT_KEY)) {
                case LOGIN_INTENT:
                    loginFacebook();
                    break;
                case SHARE_URL_INTENT:
                    if (bundle != null) {
                        AndroidUtils.showProgressDialog(this);
                        String title = bundle.getString(TITLE_KEY);
                        String description = bundle.getString(DESCRIPTION_KEY);
                        String url = bundle.getString(URL_KEY);
                        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(description) && !TextUtils.isEmpty(url)) {
                            shareLinks(this, title, description, url);
                            AndroidUtils.showProgressDialog(this);
                        }
                    }
                    break;
                case SHARE_IMAGE_INTENT:

                    if (isLoggedIn()) {
                        if (bundle != null) {
                            String title = bundle.getString(TITLE_KEY);
                            Bitmap image = bundle.getParcelable(IMAGE_KEY);
                            if (image != null && !TextUtils.isEmpty(title)) {
                                sharePhotoToFacebook(this, image, title);
                            }
                        }
                    } else {
                        showSnackBar("Please login to continue");
                    }
                    break;
                case LOGOUT_INTENT:
                    makeUserLogout(this);
                    break;
            }

        }



        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");
                        setResult(RESULT_OK);
                        finish();
                        overridePendingTransition(0, 0);

                    }

                    @Override
                    public void onCancel() {
                        showSnackBar("Login failed.Please try again");

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        showSnackBar("Login failed.Please try again");

                    }
                });

        mShareDialog.registerCallback(mCallbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(0, 0);
                AndroidUtils.hideProgressDialog();
            }

            @Override
            public void onCancel() {

                AndroidUtils.hideProgressDialog();
                showSnackBar("Url sharing failed.Please try again");

            }

            @Override
            public void onError(FacebookException error) {
                AndroidUtils.hideProgressDialog();
                showSnackBar("Url sharing failed.Please try again");

            }
        });
    }

    public void loginFacebook() {
        if (!isLoggedIn()) {
            LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        }
        else
        {
            showSnackBar("Already logged in");

        }
    }

    public void shareLinks(Activity activity, String postTitle, String description, String url) {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(postTitle)
                        .setContentDescription(
                                description)
                        .setContentUrl(Uri.parse(url))
                        .build();

                mShareDialog.show(linkContent);
            }

    }

    private void sharePhotoToFacebook(Context context, Bitmap image, String caption) {
        AndroidUtils.showProgressDialog(context);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption(caption)
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(0, 0);
                AndroidUtils.hideProgressDialog();
            }

            @Override
            public void onCancel() {

                AndroidUtils.hideProgressDialog();
                showSnackBar("Image sharing failed.Please try again");
            }

            @Override
            public void onError(FacebookException error) {
                AndroidUtils.hideProgressDialog();
                showSnackBar("Image sharing failed.Please try again");

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }

    private void makeUserLogout(Context  context){
        if (isLoggedIn()) {
            AndroidUtils.showProgressDialog(context);
            LoginManager.getInstance().logOut();
        }
        else
        {
            showSnackBar("Please login");


        }
    }

    /**
     * Shows snack bar for error condition
     *
     * @param message Error message to be shown
     */
    private void showSnackBar(String message) {

        Snackbar.make(FacebookActionActivity.this.findViewById(R.id.facebook_library_coordinator), message, Snackbar.LENGTH_LONG).show();
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                overridePendingTransition(0, 0);
            }
        },3250);

    }
}

package com.tech.bibin.imagesfetching.utils;

import android.app.Activity;
import android.content.Intent;

import com.tech.bibin.imagesfetching.model.ModelSettings;
import com.tech.bibin.imagesfetching.ui.PhotoFoldersActivity;

/**
 * Created by bibin.b on 1/2/2018.
 */

public class ImagesFolderUtils {

    private static final String TAG = "ImagesFolderUtils";
    private static ImagesFolderUtils ourInsatnce = new ImagesFolderUtils();
    private String titile = "Select Photo";
    public static final String MODEL_SETTING_KEY = "model_setting_key";


    public static ImagesFolderUtils getInstance() {
        return ourInsatnce;
    }

    public ImagesFolderUtils() {
    }

    public Intent build(Activity activity) {
        ModelSettings modelSettings = new ModelSettings();
        Intent intent = new Intent(activity, PhotoFoldersActivity.class);
        modelSettings.setTitle(titile);
        intent.putExtra(MODEL_SETTING_KEY, modelSettings);
        return intent;
    }

    public ImagesFolderUtils setTextSize(int size) {
        return ourInsatnce;
    }

    public ImagesFolderUtils setToolbarTitle(String titile) {
        this.titile = titile;
        return ourInsatnce;
    }


}

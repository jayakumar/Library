package com.tech.bibin.imagesfetching.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.tech.bibin.imagesfetching.R;
import com.tech.bibin.imagesfetching.adapters.PhotoImagesAdapterNew;
import com.tech.bibin.imagesfetching.model.ModelImages;

/***
 * Created by bibin.b on 12/28/2017.
 */

public class PhotoImagesActivity extends AppCompatActivity implements PhotoImagesAdapterNew.ImageClickCallBack, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private PhotoImagesAdapterNew photoImagesAdapterNew;
    private ModelImages modelImages;
    private SwipeRefreshLayout refreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.images_folder_list);
        modelImages = getIntent().getParcelableExtra("value");
        mInitView();
        mInitControlls();
        setImageToAdapter();
    }

    private void mInitControlls() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void mInitView() {
        recyclerView = findViewById(R.id.gv_folder);
        refreshLayout = findViewById(R.id.swipe_refresh);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setTitle("");
        }

    }

    private void setImageToAdapter() {
        if (modelImages != null) {
            photoImagesAdapterNew = new PhotoImagesAdapterNew(modelImages, this);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            recyclerView.setAdapter(photoImagesAdapterNew);
        } else return;

    }

    @Override
    public void onImageClick(int poistion) {
        Intent intent = new Intent(getApplicationContext(), FullImageActivity.class);
        intent.putExtra("path", modelImages.getAl_imagepath().get(poistion));
        startActivityForResult(intent, 1);
    }


    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null) {
                setResult(1, data);
                finish();

            }
        } else {
            return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
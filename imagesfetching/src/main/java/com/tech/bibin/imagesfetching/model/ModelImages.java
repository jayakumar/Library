package com.tech.bibin.imagesfetching.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by bibin.b on 12/28/2017.
 */

public class ModelImages implements Parcelable {
    String str_folder;
    ArrayList<String> al_imagepath;

    public ModelImages() {

    }

    protected ModelImages(Parcel in) {
        str_folder = in.readString();
        al_imagepath = in.createStringArrayList();
    }

    public static final Creator<ModelImages> CREATOR = new Creator<ModelImages>() {
        @Override
        public ModelImages createFromParcel(Parcel in) {
            return new ModelImages(in);
        }

        @Override
        public ModelImages[] newArray(int size) {
            return new ModelImages[size];
        }
    };

    public String getStr_folder() {
        return str_folder;
    }

    public void setStr_folder(String str_folder) {
        this.str_folder = str_folder;
    }

    public ArrayList<String> getAl_imagepath() {
        return al_imagepath;
    }

    public void setAl_imagepath(ArrayList<String> al_imagepath) {
        this.al_imagepath = al_imagepath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(str_folder);
        parcel.writeStringList(al_imagepath);
    }
}
package com.tech.bibin.imagesfetching.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.tech.bibin.imagesfetching.model.ModelImages;
import com.tech.bibin.imagesfetching.R;
import com.tech.bibin.imagesfetching.adapters.PhotoFolderAdapterNew;
import com.tech.bibin.imagesfetching.model.ModelSettings;

import java.util.ArrayList;

import static com.tech.bibin.imagesfetching.utils.ImagesFolderUtils.MODEL_SETTING_KEY;

/**
 * Created by bibin.b on 1/2/2018.
 */

public class PhotoFoldersActivity extends AppCompatActivity implements PhotoFolderAdapterNew.ImageFolderClickCallBack, SwipeRefreshLayout.OnRefreshListener {
    private static final int REQUEST_PERMISSIONS = 100;
    private static ArrayList<ModelImages> modelImages;
    private RecyclerView imageFolderGridView;
    boolean boolean_folder;
    private PhotoFolderAdapterNew photoFolderAdapterNew;
    private SwipeRefreshLayout refreshLayout;
    private ModelSettings modelSettings;
    private static final String TAG = "PhotoFoldersActivity";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.images_folder_list);
        mInitView();
        mAskPermission();


    }

    private void mAskPermission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        } else {
            setadpaterInList(mFoldersOfImages());
        }
    }

    private void setadpaterInList(ArrayList<ModelImages> modelImages) {
        photoFolderAdapterNew = new PhotoFolderAdapterNew(modelImages, this);
        // imageFolderGridView.setLayoutManager(new GridLayoutManager(this, 2));
        imageFolderGridView.setLayoutManager(new LinearLayoutManager(this));
        imageFolderGridView.setAdapter(photoFolderAdapterNew);
        photoFolderAdapterNew.notifyDataSetChanged();
    }

    @SuppressLint("ResourceAsColor")
    private void mInitView() {
        imageFolderGridView = findViewById(R.id.gv_folder);
        refreshLayout = findViewById(R.id.swipe_refresh);
        refreshLayout.setOnRefreshListener(this);
        modelSettings = getIntent().getParcelableExtra(MODEL_SETTING_KEY);
        modelImages = new ArrayList<>();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(modelSettings.getTitle());
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults.length > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        setadpaterInList(mFoldersOfImages());
                    } else {
                        Toast.makeText(this, "The app was not allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    public ArrayList<ModelImages> mFoldersOfImages() {
        modelImages.clear();
        int int_position = 0;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        int column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            String absolutePathOfImage = cursor.getString(column_index_data);
            for (int i = 0; i < modelImages.size(); i++) {
                if (modelImages.get(i).getStr_folder().equals(cursor.getString(column_index_folder_name))) {
                    boolean_folder = true;
                    int_position = i;
                    break;
                } else {
                    boolean_folder = false;
                }
            }
            if (boolean_folder) {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.addAll(modelImages.get(int_position).getAl_imagepath());
                al_path.add(absolutePathOfImage);
                modelImages.get(int_position).setAl_imagepath(al_path);

            } else {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.add(absolutePathOfImage);
                ModelImages obj_model = new ModelImages();
                obj_model.setStr_folder(cursor.getString(column_index_folder_name));
                obj_model.setAl_imagepath(al_path);
                modelImages.add(obj_model);
            }
        }
        return modelImages;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onImageClick(int poistion) {
        ModelImages images = modelImages.get(poistion);
        Intent intent = new Intent(getApplicationContext(), PhotoImagesActivity.class);
        intent.putExtra("value", images);
        startActivityForResult(intent,1);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if(data!=null) {
                setResult(1, data);
                finish();
            }
        }else {
            return;
        }

    }
}

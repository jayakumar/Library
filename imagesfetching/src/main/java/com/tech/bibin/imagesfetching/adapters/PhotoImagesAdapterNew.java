package com.tech.bibin.imagesfetching.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tech.bibin.imagesfetching.model.ModelImages;
import com.tech.bibin.imagesfetching.R;


/**
 * Created by bibin.b on 1/4/2018.
 */

public class PhotoImagesAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ModelImages modelImages;
    private ImageClickCallBack imageClickCallBack;

    public interface ImageClickCallBack {
        void onImageClick(int poistion);
    }


    public PhotoImagesAdapterNew(ModelImages modelImages, ImageClickCallBack imageFolderClickCallBack) {
        this.modelImages = modelImages;
        this.imageClickCallBack =imageFolderClickCallBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.folder_imageview, viewGroup, false);
        return new ImageFolderViewHolder(view, imageClickCallBack);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ImageFolderViewHolder) {
            ImageFolderViewHolder imageFolderViewHolder = (ImageFolderViewHolder) holder;
            setImage(holder, position, imageFolderViewHolder);
        }
    }


    private void setImage(RecyclerView.ViewHolder holder, int position, ImageFolderViewHolder imageFolderViewHolder) {
        Glide.with(holder.itemView.getContext())
                .load(modelImages.getAl_imagepath().get(position))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .skipMemoryCache(true)
                .into(imageFolderViewHolder.thumbNail);
    }


    @Override
    public int getItemCount() {
        if (modelImages.getAl_imagepath().size() > 0) {
            return modelImages.getAl_imagepath().size();
        } else {
            return 1;
        }
    }

    static class ImageFolderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView thumbNail;
        private final ImageClickCallBack imageFolderClickCallBack;


        public ImageFolderViewHolder(View view, ImageClickCallBack imageFolderClickCallBack) {
            super(view);
            thumbNail = view.findViewById(R.id.iv_image);
            this.imageFolderClickCallBack = imageFolderClickCallBack;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            imageFolderClickCallBack.onImageClick(getAdapterPosition());

        }
    }
}

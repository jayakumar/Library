package com.tech.bibin.imagesfetching.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bibin.b on 1/5/2018.
 */

public class ModelSettings implements Parcelable {
    private String title;

    public ModelSettings() {
    }

    protected ModelSettings(Parcel in) {
        title = in.readString();
    }

    public static final Creator<ModelSettings> CREATOR = new Creator<ModelSettings>() {
        @Override
        public ModelSettings createFromParcel(Parcel in) {
            return new ModelSettings(in);
        }

        @Override
        public ModelSettings[] newArray(int size) {
            return new ModelSettings[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.tech.bibin.imagesfetching.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.tech.bibin.imagesfetching.R;

public class FullImageActivity extends AppCompatActivity implements View.OnClickListener {
    private TouchImageView mTouchImageView;
    private static final String TAG = "FullScreenActivity";
    private FloatingActionButton fab;
    private String path;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image_shows);
        mTouchImageView = findViewById(R.id.image_full);
        bundle = getIntent().getExtras();
        mInitView();
        mInitContrls();


    }

    private void mInitContrls() {
        fab.setOnClickListener(this);
    }

    private void mInitView() {
        fab = findViewById(R.id.fab);
        if (bundle != null) {
            path = getIntent().getExtras().getString("path");
            mTouchImageView.setImage(path, mTouchImageView);

        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
    }

    @Override
    public void onClick(View view) {
        if (view == fab) {
            Intent intent = new Intent();
            if (path.isEmpty()) {
                Snackbar.make(view, "Selection Failed", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }
            intent.putExtra("path", path);
            setResult(1, intent);
            finish();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();

        }
        return super.onOptionsItemSelected(item);
    }
}

package com.tech.bibin.imagesfetching.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tech.bibin.imagesfetching.model.ModelImages;
import com.tech.bibin.imagesfetching.R;

import java.util.ArrayList;

/**
 * Created by bibin.b on 1/4/2018.
 */

public class PhotoFolderAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ModelImages> modelImages;
    private ImageFolderClickCallBack imageFolderClickCallBack;

    public interface ImageFolderClickCallBack {
        void onImageClick(int poistion);
    }


    public PhotoFolderAdapterNew(ArrayList<ModelImages> modelImages, ImageFolderClickCallBack imageFolderClickCallBack) {
        this.modelImages = modelImages;
        this.imageFolderClickCallBack=imageFolderClickCallBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.folder_list_adapter, viewGroup, false);
        return new ImageFolderViewHolder(view,imageFolderClickCallBack);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ImageFolderViewHolder) {
            ImageFolderViewHolder imageFolderViewHolder = (ImageFolderViewHolder) holder;
            setImage(holder, position, imageFolderViewHolder);
            imageFolderViewHolder.folderName.setText(modelImages.get(position).getStr_folder());
            imageFolderViewHolder.countImages.setText(modelImages.get(position).getAl_imagepath().size() + "");

        }
    }


    private void setImage(RecyclerView.ViewHolder holder, int position, ImageFolderViewHolder imageFolderViewHolder) {
        Glide.with(holder.itemView.getContext())
                .load(modelImages.get(position).getAl_imagepath().get(0))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageFolderViewHolder.thumbNail);
    }


    @Override
    public int getItemCount() {
        return modelImages.size();
    }

    static class ImageFolderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView thumbNail;
        private final TextView folderName;
        private final TextView countImages;
        private final ImageFolderClickCallBack imageFolderClickCallBack;


        public ImageFolderViewHolder(View view, ImageFolderClickCallBack imageFolderClickCallBack) {
            super(view);
            thumbNail = view.findViewById(R.id.folder_images);
            folderName = view.findViewById(R.id.folde_names);
            countImages = view.findViewById(R.id.image_counts);
            this.imageFolderClickCallBack = imageFolderClickCallBack;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            imageFolderClickCallBack.onImageClick(getAdapterPosition());
        }
    }
}

**Android Reusable Components**
==========

ReusableLibrary package contains useful tools (often merged in from sources) for tasks such as Gallery, Share and so forth. While the core API is likely to remain small over time, dev section will grow and evolve as more use-cases are solved and added.

The directory contains util components that are coupled to make it reusable across multiple projects. This is created by jinu and is published under free licence 


## API documentation
The following list contains all major sections of ReusableComponentLibrary’s prose API documentation, which expands upon the concepts outlined in the overview and also covers advanced topics.

   - [Zoom Image](#zoom-image)
   - [ErrorProgressView](#errorprogressview)
   - [Image Attacher (From Gallery/Camera)](#image-attacher-from-gallerycamera)
   - [Version Updater](#version-updater)
   - [Twitter (Integration/Login/Share)](#twitter-integrationloginshare)) 
   - [Google+ (Integration/Login/Share)](#google-integrationloginshare)
   - [Facebook (Integration/Login/Share)](#facebook-integrationloginshare) 
   - [Image Slider](#image-slider)
   - [Text Validator](#text-validator)




### Zoom Image

Usage:
import module imageattacher to your project 
and add below dependency in your app **build.gradle**
```gradle
compile project(':imageattacher')
```
View Full size image with few lines of code.

```java
ZoomImageUtils
.getInstance()
.setTitle(title)
.setUrl(url)
.setFile(false)
.build(imageView, getActivity());
```
**Screen shots**    
![](readme/readmeimage_zoom.gif)   


### ErrorProgressView

Usage:
import module errorprogress to your project 
and add below dependency in your app **build.gradle**
```gradle
compile project(':errorprogress')
```

This library includes progress and error conditions that can be shown in users screen.

Add ErrorProgressView in your layout.

```xml
 <com.jinu.errorprogress.ErrorProgressView
        android:id="@+id/error_view"
        android:layout_centerVertical="true"
        android:layout_centerHorizontal="true"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
```

Set it to true to see the errors. 

```java
mErrorView.showshowProgress(true);
```

Show an error view of empty list add below code.
```java
mErrorView.setErrorType(ErrorProgressView.TYPE_EMPTY);
mErrorView.setErrorType(ErrorProgressView.TYPE_EMPTY, message, buttonText)
```
Show network error.
```java
mErrorView.setErrorType(ErrorProgressView.TYPE_NETWORK);
mErrorView.setErrorType(ErrorProgressView.TYPE_NETWORK, message, buttonText)
```

Shows location not available.
```java
mErrorView.setErrorType(ErrorProgressView.TYPE_LOCATION);
mErrorView.setErrorType(ErrorProgressView.TYPE_LOCATION, message, buttonText)
```

To access button click of inline view initialize interface in your code.
```java
 mErrorProgressView.setOnRetryAction(new ErrorProgressView.OnRetryAction() {
 
      @Override
      public void onRetry() {
                //todo add your code here
      }
  });
```
**Screen shots**    
![](readme/error_progress_view.gif)     

### Image Attacher (From Gallery/Camera)

Usage:
import module imageattacher to your project and add dependency in your app **build.gradle**
```gradle
compile project(':imageattacher')
```
The library is compiled with Android Marshmallow and update your build script to 23. You don't have to add any permission, for all permissions are handled internally.

**Features**  
1. Crop   
2. Rotate
3. Saves in specifed dir
4. Comress image
5. 


```java
ImagePicker.getInstance()
.setFolderName(<folder name to save>)
.useStorageApi(<uses storage api enabeled above ktikat devices>)
.setCompressWidthHeight(width,height)
.useGallery(<true if you want to take image from gallery else opens up camera>)
.positiveButtonName(<name of save button >)
.build(context)
```

The above code will return intent you need to call.
```java
 startActivityForResult()
```
startActivityForResult() is invoked to get processed image.

Result will be given from the activity result.
```java 
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data){
    String fileLocation = data.getData().getPath();
    imageView.setImageBitmap(ImageUtils.decodeBitmap(fileLocation, 100, 100));
  }

```

**Screen shots**    
![](readme/splash.png)     

    

### Version Updater

Usage:
import module versioncheck to your project and add dependency in your app **build.gradle**
```gradle
compile project(':versioncheck')
```

Version Checker will check from the server whether your app is up to date or not. If the current version seems to be outdated, it will promt the user to update to the latest version. 
App lock is added if the user version can no longer be used.  

To start service use the below code, 

```java
VersionCheckUtils
.getInstance()
.setProjectName( project name )
.setBundleId(getActivity().getPackageName())
.setVersionCode(BuildConfig.VERSION_CODE)
.setUrl(url)
.build(context);
```

Use your business logic after verifying. 

```java
if (UpdateUtils.isUpdatedVersion(getActivity(), BuildConfig.VERSION_CODE)) {
         startMainActivity();
         startService();

   } else {
         if (UpdateUtils.isLock(getActivity())) {
              startActivity(new Intent(getActivity(), UpdateAppActivity.class));
              return;
         }
         if (!PreferenceUtils.getSkip(getActivity())) {
              startMainActivity();
              startService();
         }
}
```

**Screen shots**    
![](readme/version.png)     

    


### Twitter (Integration/Login/Share)

Usage:
import module reusablecomponent to your project and add dependency in your app **build.gradle**
```gradle
compile project(':reusablecomponent')
```

To login to Twiter use below code
```java
TwitterConfig twitterConfig = new TwitterConfig();
twitterConfig.action = ConstantValues.Action.ACTION_LOGIN;
OAuthActivity.startTwitter(getActivity(), twitterConfig);
```

Sharing goes like this.

```java
TwitterConfig twitterConfig = new TwitterConfig();
twitterConfig.action = ConstantValues.Action.ACTION_SHARE;
twitterConfig.fileLocation = fileT;
twitterConfig.text = "Hi all.........";
OAuthActivity.startTwitter(getActivity(), twitterConfig);
```

  


### Google+ (Integration/Login/Share)

Usage:
import module reusablecomponent to your project and add dependency in your app **build.gradle**
```gradle
compile project(':reusablecomponent')
```

For Google Plus login use below code

```java
GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
googlePlusConfig.action = ConstantValues.Action.ACTION_LOGIN;
GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
```

G+ logout  
```java
GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
googlePlusConfig.action = ConstantValues.Action.ACTION_LOGOUT;
GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
```

G+ share 
```java
GooglePlusConfig googlePlusConfig = new GooglePlusConfig();
googlePlusConfig.action = ConstantValues.Action.ACTION_SHARE;
googlePlusConfig.fileLocation = fileLocation; // dont have any file pass it as null
googlePlusConfig.text = text;
GoogleActionActivity.startGooglePlus(getActivity(), googlePlusConfig);
```
  


### Facebook (Integration/Login/Share) 

Usage:
import module **reusablecomponent** to your project and add below dependency in your app **build.gradle**
```gradle
compile project(':reusablecomponent')
```

Use the below code for Facebook login.

```java
startActivityForResult(
FacebookActionActivity.getLoginIntent(getActivity()), 
LOGIN_REQUEST_CODE);
```
After successful login data will be returned via **onActivityResult**


Logout  
```java
startActivityForResult(
FacebookActionActivity.getLogoutIntent(getActivity()), 
LOGOUT_REQUEST_CODE);
```


Share image and text
```java
startActivity(FacebookActionActivity
.getShareImageIntent(getActivity(),file,text));
```

Share a link
```java
startActivityForResult(FacebookActionActivity.getShareLinkIntent(getActivity(), 
heading, text, url), 
SHARE_URL_REQUEST_CODE);
```


### Image Slider

Usage:
import module reusablecomponent to your project 
and add below dependency in your app **build.gradle**
```gradle
compile project(':reusablecomponent')
```
It has got a lot of features, which you can use to create your own image gallery. 

If you have multiple images in server and you want to show it in a gallery 
use this and add your links in an **ArrayList** and pass it to library.

**Sample code**
```java
ArrayList<String> imageArrayList = new ArrayList<>();
imageArrayList.add("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTfZz_-WmmixVCxZfdvdWNTs6Yf2JwgFMOYuMkyt5-wUtEDHHpSEQ");
imageArrayList.add("https://static-secure.guim.co.uk/sys-images/Sport/Pix/pictures/2010/12/19/1292778133565/Sachin-Tendulkar-India-So-007.jpg");
imageArrayList.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTMvLzOic1Hf7odoK7Htqxrtp3am0JpU8QuKBp7RXG1zuF9EgA9PA");
ImageSliderActivity.startIamgeSlider(getActivity(), imageArrayList);
```



### Text Validator

Usage:
import module reusablecomponent to your project 
and add below dependency in your app **build.gradle**
```gradle
compile project(':reusablecomponent')
```

This library is used to validate EditText and some regular expressions.

Contains validation of name, phone number and email.

For email 
```java 
Validation.email(EditText)
```

For name
```java 
Validation.name(EditText)
```

For phoneNo
```java 
Validation.phone(EditText)
```

Library contains some general checks like
isAlphaNumeric, containsOnlyNumbers, etc.

Methods will return an integer value.
value 1 is INVALID invalid input  
value 2 is EMPTY value is empty  
value 3 is EMPTY_VIEW if passed EditText is null  
value 4 is OK value is valid  





**Authors**
---------------------------------------------
Zoom Image by [jinu](https://plus.google.com/+JINUJAYAKUMAR27)  
ErrorProgressView by [jinu](https://plus.google.com/+JINUJAYAKUMAR27)  
Image Attacher (From Gallery/Camera) by [jinu](https://plus.google.com/+JINUJAYAKUMAR27)  
Version Updater by [jinu](https://plus.google.com/+JINUJAYAKUMAR27)  
Twitter (Integration/Login/Share) by [nidheesh](https://plus.google.com/+Nidheeshkv9746222048)  
Google+ (Integration/Login/Share) by [nidheesh](https://plus.google.com/+Nidheeshkv9746222048)  
Facebook (Integration/Login/Share) by [nidheesh](https://plus.google.com/+Nidheeshkv9746222048)  
Image Slider by [nidheesh](https://plus.google.com/+Nidheeshkv9746222048)  
Text Validator by [jinu](https://plus.google.com/+JINUJAYAKUMAR27)
package com.jinu.errorprogress;

import android.content.Context;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/***
 * Created by Jinu on 3/25/2016.
 ***/
public class ErrorProgressView extends LinearLayoutCompat implements View.OnClickListener {

    private ContentLoadingProgressBar mProgressBar;
    private Button mRetryButton;
    private TextView mErrorTitleTxt;
    private TextView mErrorMessageTxt;
    private AppCompatImageView mErrorImage;
    private OnRetryAction onRetryAction;

    public static final int TYPE_LOCATION = 0;

    public static final int TYPE_EMPTY = 1;

    public static final int TYPE_NETWORK = 2;

    public static final int TYPE_NONE = -1;

    @Override
    public void onClick(View v) {
        if (onRetryAction != null) {
            onRetryAction.onRetry();
        }

    }

    public interface OnRetryAction {
        void onRetry();
    }

    public ErrorProgressView(Context context) {
        super(context);
        setOrientation(LinearLayoutCompat.VERTICAL);
        init(context);
    }

    public ErrorProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayoutCompat.VERTICAL);
        init(context);
    }

    public ErrorProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(LinearLayoutCompat.VERTICAL);
        init(context);
    }

    protected void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.error_progress_view, this);
        mProgressBar = findViewById(R.id.error_progress);
        mRetryButton = findViewById(R.id.error_retry_btn);
        mErrorTitleTxt = findViewById(R.id.error_title);
        mErrorMessageTxt = findViewById(R.id.error_message);
        mErrorImage = findViewById(R.id.error_image);
        mRetryButton.setOnClickListener(this);
    }

    public void showProgress(boolean show) {
        if (show) {
            mProgressBar.setVisibility(VISIBLE);
            showImage(false);
            showButton(false);
            mErrorTitleTxt.setVisibility(GONE);
            mErrorMessageTxt.setVisibility(GONE);
        } else {
            mProgressBar.setVisibility(GONE);
        }
    }

    public void setErrorType(int type) {

        switch (type) {
            case TYPE_EMPTY:
                showError(R.drawable.error_view_empty, R.string.error_view_empty_list, R.string.error_view_network_text);
                break;
            case TYPE_NETWORK:
                showError(R.drawable.error_view_network, R.string.error_view_no_network_available, R.string.error_view_network_text);
                break;
            case TYPE_LOCATION:
                showError(R.drawable.error_view_gps_off, R.string.error_view_gps_of, R.string.error_view_gs_text);
                break;
            default:
                showError(R.drawable.error_view_general_error, R.string.error_view_general_error, R.string.error_view_network_text);
                break;
        }

    }

    private void showError(int error_view_empty, int error_view_empty_list, int error_view_network_text) {
        showProgress(false);
        showButton(true);
        showImage(true);
        mErrorTitleTxt.setVisibility(VISIBLE);
        mErrorImage.setImageResource(error_view_empty);
        setErrorMessageText(getContext().getString(error_view_empty_list));
        setButtonText(getContext().getString(error_view_network_text));
    }

    public void setErrorType(int type, String message, String buttonText) {

        switch (type) {
            case TYPE_EMPTY:
                showError(message, buttonText, R.drawable.error_view_empty);
                break;
            case TYPE_NETWORK:
                showError(message, buttonText, R.drawable.error_view_network);
                break;
            case TYPE_LOCATION:
                showError(message, buttonText, R.drawable.error_view_gps_off);
                break;
            default:
                showError(message, buttonText, R.drawable.error_view_general_error);
                break;
        }
    }

    private void showError(String message, String buttonText, int error_view_empty) {
        showProgress(false);
        showButton(true);
        showImage(true);
        mErrorTitleTxt.setVisibility(GONE);
        mErrorImage.setImageResource(error_view_empty);
        setErrorMessageText(message);
        setButtonText(buttonText);
    }

    public void setErrorMessageText(String mErrorMessageTxt) {
        showView(this.mErrorMessageTxt);
        this.mErrorMessageTxt.setText(mErrorMessageTxt);
    }

    private void showView(View mErrorMessageTxt) {
        mErrorMessageTxt.setVisibility(VISIBLE);
    }

    public void setErrorTitleText(String mErrorTitleTxt) {
        showView(this.mErrorTitleTxt);
        this.mErrorTitleTxt.setText(mErrorTitleTxt);
    }

    public void setButtonText(String mRetryButton) {
        showView(this.mRetryButton);
        this.mRetryButton.setText(mRetryButton);
    }

    public void showButton(boolean show) {
        if (show) {
            mRetryButton.setVisibility(VISIBLE);
        } else {
            mRetryButton.setVisibility(GONE);
        }
    }

    public void showImage(boolean show) {
        if (!show) {
            mErrorImage.setVisibility(GONE);
        } else {
            mErrorImage.setVisibility(VISIBLE);
        }
    }

    public void showErrorView(String title, String message) {
        showProgress(false);
        showButton(true);
        showView(mErrorImage);
        setErrorTitleText(title);
        setErrorMessageText(message);
    }


    public void setOnRetryAction(OnRetryAction onRetryAction) {
        this.onRetryAction = onRetryAction;
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }
}
